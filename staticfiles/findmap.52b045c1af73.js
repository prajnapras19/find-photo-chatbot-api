function loadButtonMap(query, request_number) {
    query = query.replace(/^\s+|\s+$/g, '');
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            chatBox = document.querySelectorAll(".mapbutton");
            res = this.responseText;
            for (let i = 0; i < chatBox.length; i++) {
                if (!chatBox[i].classList.contains('occupied')) {
                    chatBox[i].innerHTML = res;
                    chatBox[i].className += "occupied";
                    break;
                }
            }
        }
    };
    xhttp.open("GET", "https://ferry-chatbot-api.herokuapp.com/mapbutton/" + query, true);
    xhttp.send();
}