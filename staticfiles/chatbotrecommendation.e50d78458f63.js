function loadChatbot(query, request_number) {
    query = query.replace(/^\s+|\s+$/g, '');
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            chatBox = document.querySelectorAll(".chatbot-recommendation");
            res = this.responseText;
            let x = 0;
            for (let i = 0; i < chatBox.length; i++) {
                if (x == 0 && !chatBox[i].classList.contains('occupied')) {
                    chatBox[i].innerHTML = res;
                    chatBox[i].className += "occupied";
                    x = 1;
                } else {
                    chatBox[i].innerHTML = '<i>Searching the best recommendation for you. Please wait...</i>&#128512';
                }
            }
        }
    };
    xhttp.open("GET", "https://ferry-chatbot-api.herokuapp.com/chatbots/" + query, true);
    xhttp.send();
}