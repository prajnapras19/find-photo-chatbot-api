from django.shortcuts import render
from find_photo_by_hashtag.models import Query
import requests
import json
import random
import os

NUM_POSTS = 2
cookies = {'sessionid': os.getenv('INSTASESSION')}

def get_posts_by_hashtags(user, tag, request_number):
    tag = tag[:]
    tag = tag.replace(' ', '')
    if (len(tag) <= 9):
        tag += 'indonesia'
    # check if it is available in database
    query = Query.objects.filter(user=user, tag=tag, request_number=request_number)
    if (len(query) > 0):
        res = []
        res.append((query[0].shortcode1, {'display_url': query[0].display_url1}))
        res.append((query[0].shortcode2, {'display_url': query[0].display_url2}))
        return res

    # otherwise, find it from instagram
    try:
        url = 'https://www.instagram.com/explore/tags/%s/?__a=1' % tag

        response = requests.get(url, cookies=cookies)
        #print(url)
        response = json.loads(response.text)

        collected_posts = dict()
        for post in response['graphql']['hashtag']['edge_hashtag_to_top_posts']['edges']: # top posts
            if (not(post['node']['is_video'])): # just get photo
                collected_posts[post['node']['shortcode']] = {'likes': post['node']['edge_liked_by']['count'], 'display_url': post['node']['display_url']}
        for post in response['graphql']['hashtag']['edge_hashtag_to_media']['edges']: # recent posts
            if (not(post['node']['is_video'])): # just get photo
                collected_posts[post['node']['shortcode']] = {'likes': post['node']['edge_liked_by']['count'], 'display_url': post['node']['display_url']}
        posts_to_be_sorted = [(a, b) for a, b in collected_posts.items()]
        posts_to_be_sorted = sorted(posts_to_be_sorted, key=lambda x:x[1]['likes'], reverse=True)
        posts_to_be_sorted = posts_to_be_sorted[:20]
        res = random.sample(posts_to_be_sorted, min(len(posts_to_be_sorted), NUM_POSTS))
        obj = Query.objects.create(user=user, tag=tag, request_number=request_number, shortcode1=res[0][0], display_url1=res[0][1]['display_url'], shortcode2=res[1][0], display_url2=res[1][1]['display_url'])
        obj.save()
        return res
    except:
        return None

def parse_to_url(shortcode_list):
    url = 'https://instagram.com/p/'
    res = []
    for shortcode in shortcode_list:
        res.append(url + shortcode + '/')
    return res

def find_photo_by_hashtag_views(request, user=None, tag=None, request_number=1):
    response = {'posts':[], 'keyword':tag}
    if (user == None or tag == None or request_number < 1):
        return render(request, 'find_photo_by_hashtag/hashtag.html', response)
    
    posts = get_posts_by_hashtags(user, tag, request_number)
    if (posts != None):
        display_urls = [post[1]['display_url'] for post in posts]
        shortcodes = parse_to_url([post[0] for post in posts])
        res = []
        for i in range(len(display_urls)):
            res.append((display_urls[i], shortcodes[i]))
        response['posts'] = res
    #print(response)
    return render(request, 'find_photo_by_hashtag/hashtag.html', response)