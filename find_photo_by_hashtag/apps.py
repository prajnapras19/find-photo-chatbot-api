from django.apps import AppConfig


class FindPhotoByHashtagConfig(AppConfig):
    name = 'find_photo_by_hashtag'
