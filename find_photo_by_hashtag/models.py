from django.db import models

class Query(models.Model):
    user = models.CharField(max_length=100)
    tag = models.CharField(max_length=100)
    request_number = models.PositiveIntegerField()
    shortcode1 = models.CharField(max_length=100)
    display_url1 = models.CharField(max_length=255)
    shortcode2 = models.CharField(max_length=100)
    display_url2 = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return str(self.tag) + ' - ' + str(self.user) + ' - request ' + str(self.request_number)