from django.contrib import admin
from find_photo_by_hashtag.models import Query

class CreatedAtAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at',)

admin.site.register(Query, CreatedAtAdmin)