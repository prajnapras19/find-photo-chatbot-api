from django.db import models

class Provinsi(models.Model):
    nama = models.CharField(max_length=50)
    def __str__(self):
        return str(self.nama)

class Kabupaten(models.Model):
    provinsi = models.ForeignKey(Provinsi, on_delete=models.CASCADE)
    nama = models.CharField(max_length=50)
    def __str__(self):
        return str(self.nama) + ", " + str(self.provinsi.nama)

class Finance(models.Model):
    provinsi = models.ForeignKey(Provinsi, on_delete=models.CASCADE)
    kabupaten = models.ForeignKey(Kabupaten, on_delete=models.CASCADE)
    makanan_minimum = models.CharField(max_length=20, default='I don\'t know')
    makanan_maximum = models.CharField(max_length=20, default='I don\'t know')
    penginapan_backpacker = models.CharField(max_length=20, default='I don\'t know')
    penginapan_ekonomis = models.CharField(max_length=20, default='I don\'t know')
    penginapan_luxury = models.CharField(max_length=20, default='I don\'t know')

    def __str__(self):
        return str(self.provinsi.nama) + ' - ' + str(self.kabupaten.nama)

class FinanceQuery(models.Model):
    query = models.CharField(max_length=100)
    provinsi = models.ForeignKey(Provinsi, on_delete=models.CASCADE)
    kabupaten = models.ForeignKey(Kabupaten, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return str(self.query) + ' - ' + str(self.provinsi) + ' - ' + str(self.kabupaten)