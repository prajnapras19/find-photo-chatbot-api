from django.apps import AppConfig


class FindFinanceConfig(AppConfig):
    name = 'find_finance'
