from django.contrib import admin
from find_finance.models import Provinsi, Kabupaten, Finance, FinanceQuery

class CreatedAtAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at',)

admin.site.register(Provinsi)
admin.site.register(Kabupaten)
admin.site.register(Finance)
admin.site.register(FinanceQuery, CreatedAtAdmin)