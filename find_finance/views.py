from django.shortcuts import render
from django.http import HttpResponse
from fuzzywuzzy import fuzz
from operator import itemgetter
from find_finance.models import Provinsi, Kabupaten, Finance, FinanceQuery
from find_keyword.views import getProvAndCity, getCoordinate_geopy, findCoordinate

errorResponse = '''Sorry, we don't have any suggestions about that right now. Please come back later or input something else &#128513;'''

# use fuzzywuzzy to get the probability that string is matched with the target
def check_probability_string(target, the_string):
    return fuzz.token_set_ratio(target, the_string)

# get the highest probable string from the 'probable' list
def max_probability(probable):
    try:
        return max(probable, key = itemgetter(1))
    except:
        return None

def findFinanceRecommendation(request, query):
    try:
        response = {}

        # check if query has been called in database
        finance_query = FinanceQuery.objects.all()
        probable_query = []
        for fq in finance_query:
            probable_query.append([fq, check_probability_string(query, fq.query)])
        probable_query = max_probability(probable_query)
        if (probable_query != None and probable_query[1] >= 70):
            provinsi = probable_query[0].provinsi
            kabupaten = probable_query[0].kabupaten
        else:
            # get provinsi and kabupaten
            specificToGlobal = getProvAndCity(getCoordinate_geopy(query))
            if (specificToGlobal == None):
                specificToGlobal = getProvAndCity(findCoordinate(query + ' indonesia'))
            provinsi, kabupaten = specificToGlobal

            # find the closest match in database
            list_provinsi = Provinsi.objects.all()
            probable_provinsi = []
            for prov in list_provinsi:
                probable_provinsi.append([prov, check_probability_string(provinsi, prov.nama)])
            provinsi = max_probability(probable_provinsi)
            assert provinsi[1] >= 70 # 70% matched
            provinsi = provinsi[0]

            list_kabupaten = Kabupaten.objects.filter(provinsi=provinsi)
            probable_kabupaten = []
            for kab in list_kabupaten:
                probable_kabupaten.append([kab, check_probability_string(kabupaten, kab.nama)])
            kabupaten = max_probability(probable_kabupaten)
            if (kabupaten[1] >= 70): # 70% matched
                kabupaten = kabupaten[0]
            else:
                kabupaten = kabupaten[0] # to be edited, kabupaten = 'general'

            # save in database
            FinanceQuery.objects.create(query=query, provinsi=provinsi, kabupaten=kabupaten)

        finance = Finance.objects.filter(provinsi=provinsi, kabupaten=kabupaten)[0]
        response['makanan_minimum'] = finance.makanan_minimum
        response['makanan_maximum'] = finance.makanan_maximum
        response['penginapan_backpacker'] = finance.penginapan_backpacker
        response['penginapan_ekonomis'] = finance.penginapan_ekonomis
        response['penginapan_luxury'] = finance.penginapan_luxury
        return render(request, 'find_finance/finance.html', response)
    except:
        return HttpResponse(errorResponse)

def financeToDB(request):
    if (not(request.user.is_staff)):
        return HttpResponse('ERROR')
    if (request.method == 'POST'):
        try:
            # normalize
            f = request.FILES['finance']
            assert f.name[-4:] == '.csv'
            chunk = f.chunks()
            financedata = ''
            while True:
                try:
                    financedata = next(chunk).decode('utf-8').strip()
                except:
                    break
            financedata = financedata.split('\n')
            # insert to db
            for line in financedata:
                line = line.strip()
                data = line.split(';')

                provinsi = Provinsi.objects.filter(nama=data[0])
                if (len(provinsi) == 0):
                    provinsi = Provinsi.objects.create(nama=data[0])
                else:
                    provinsi = provinsi[0]

                kabupaten = Kabupaten.objects.filter(provinsi=provinsi, nama=data[1])
                if (len(kabupaten) == 0):
                    kabupaten = Kabupaten.objects.create(provinsi=provinsi, nama=data[1])
                else:
                    kabupaten = kabupaten[0]

                finance = Finance.objects.filter(provinsi=provinsi, kabupaten=kabupaten)            
                if (len(finance) == 0):
                    Finance.objects.create(provinsi=provinsi, kabupaten=kabupaten, makanan_minimum=data[2], makanan_maximum=data[3], penginapan_backpacker=data[4], penginapan_ekonomis=data[5], penginapan_luxury=data[6])
                else:
                    finance = finance[0]
                    finance.makanan_minimum = data[2]
                    finance.makanan_maximum = data[3]
                    finance.penginapan_backpacker = data[4]
                    finance.penginapan_ekonomis = data[5]
                    finance.penginapan_luxury = data[6]
                    finance.save()
        
        except:
            return HttpResponse('ERROR')
        return HttpResponse('OK')
    return render(request,'find_finance/add_data_finance.html')