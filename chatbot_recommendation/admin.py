from django.contrib import admin
from chatbot_recommendation.models import Chatbot, ChatbotQuery

class CreatedAtAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at',)

admin.site.register(Chatbot)
admin.site.register(ChatbotQuery, CreatedAtAdmin)