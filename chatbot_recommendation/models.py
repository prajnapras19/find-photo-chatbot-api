from django.db import models
from find_finance.models import Kabupaten

class Chatbot(models.Model):
    nama = models.CharField(max_length=50)
    url = models.URLField()
    kabupaten = models.ManyToManyField(Kabupaten, blank=True)

    def __str__(self):
        return str(self.nama)

class ChatbotQuery(models.Model):
    query = models.CharField(max_length=100)
    kabupaten = models.ForeignKey(Kabupaten, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return str(self.query) + ' - ' + str(self.kabupaten)