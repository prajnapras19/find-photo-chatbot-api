from django.apps import AppConfig


class ChatbotRecommendationConfig(AppConfig):
    name = 'chatbot_recommendation'
