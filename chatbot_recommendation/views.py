from django.shortcuts import render
from django.http import HttpResponse
from chatbot_recommendation.models import Chatbot, ChatbotQuery
from find_finance.models import Provinsi, Kabupaten
from find_keyword.views import getProvAndCity, getCoordinate_geopy, findCoordinate
from fuzzywuzzy import fuzz
from operator import itemgetter

# use fuzzywuzzy to get the probability that string is matched with the target
def check_probability_string(target, the_string):
    return fuzz.token_set_ratio(target, the_string)

# get the highest probable string from the 'probable' list
def max_probability(probable):
    try:
        return max(probable, key = itemgetter(1))
    except:
        return None

def chatbotRecommendation(request, query):
    try:
        response = {}
        response['chatbots'] = []
        provinsi = ''
        kabupaten = ''

        # check if query has been called in database
        chatbot_query = ChatbotQuery.objects.all()
        probable_query = []
        for cq in chatbot_query:
            probable_query.append([cq, check_probability_string(query, cq.query)])
        probable_query = max_probability(probable_query)

        if (probable_query != None and probable_query[1] >= 70):
            kabupaten = probable_query[0].kabupaten
            provinsi = kabupaten.provinsi
            # get chatbot
            chatbots = Chatbot.objects.filter(kabupaten=kabupaten)
            for chatbot in chatbots:
                response['chatbots'].append(chatbot)
        else:
            # get provinsi and kabupaten
            specificToGlobal = getProvAndCity(getCoordinate_geopy(query))
            if (specificToGlobal == None):
                specificToGlobal = getProvAndCity(findCoordinate(query + ' indonesia'))
            provinsi, kabupaten = specificToGlobal

            # find the closest match in database
            list_provinsi = Provinsi.objects.all()
            probable_provinsi = []
            for prov in list_provinsi:
                probable_provinsi.append([prov, check_probability_string(provinsi, prov.nama)])
            provinsi = max_probability(probable_provinsi)
            assert provinsi[1] >= 70 # 70% matched
            provinsi = provinsi[0]

            list_kabupaten = Kabupaten.objects.filter(provinsi=provinsi)
            probable_kabupaten = []
            for kab in list_kabupaten:
                probable_kabupaten.append([kab, check_probability_string(kabupaten, kab.nama)])
            kabupaten = max_probability(probable_kabupaten)
            if (kabupaten[1] >= 70): # 70% matched
                kabupaten = kabupaten[0]
                # save in database
                ChatbotQuery.objects.create(query=query, kabupaten=kabupaten)

                # get chatbot
                chatbots = Chatbot.objects.filter(kabupaten=kabupaten)
                for chatbot in chatbots:
                    response['chatbots'].append(chatbot)

        kabupaten = Kabupaten.objects.filter(provinsi=provinsi, nama='general')
        if (len(kabupaten) > 0):
            chatbots = Chatbot.objects.filter(kabupaten=kabupaten[0])
            for chatbot in chatbots:
                if (chatbot not in response['chatbots']):
                   response['chatbots'].append(chatbot)

        return render(request, 'chatbot_recommendation/chatbot.html', response)
    except:
        return render(request, 'chatbot_recommendation/chatbot.html')

def chatbotToDB(request):
    if (not(request.user.is_staff)):
        return HttpResponse('ERROR')
    if (request.method == 'POST'):
        try:
            # normalize
            f = request.FILES['finance']
            assert f.name[-4:] == '.csv'
            chunk = f.chunks()
            chatbotdata = ''
            while True:
                try:
                    chatbotdata = next(chunk).decode('utf-8').strip()
                except:
                    break
            chatbotdata = chatbotdata.split('\n')
            # insert to db
            for line in chatbotdata:
                line = line.strip()
                data = line.split(';')

                provinsi = Provinsi.objects.filter(nama=data[2])
                if (len(provinsi) == 0):
                    provinsi = Provinsi.objects.create(nama=data[2])
                else:
                    provinsi = provinsi[0]

                kabupaten = Kabupaten.objects.filter(provinsi=provinsi, nama=data[3])
                if (len(kabupaten) == 0):
                    kabupaten = Kabupaten.objects.create(provinsi=provinsi, nama=data[3])
                else:
                    kabupaten = kabupaten[0]

                chatbot = Chatbot.objects.filter(nama=data[0], url=data[1])
                if (len(chatbot) == 0):
                    chatbot = Chatbot.objects.create(nama=data[0], url=data[1])
                else:
                    chatbot = chatbot[0]
                chatbot.kabupaten.add(kabupaten)
        except:
            return HttpResponse('ERROR')
        return HttpResponse('OK')
    return render(request,'chatbot_recommendation/add_data_chatbot.html')