from django.shortcuts import render
from django.http import HttpResponse
from find_photo_by_hashtag.models import Query
from find_keyword.models import KeywordCoordinate, KeywordGlobalToSpesific
from find_finance.models import FinanceQuery
from chatbot_recommendation.models import ChatbotQuery


def deleteDatabase(request):
    if (not(request.user.is_staff)):
        return HttpResponse('ERROR')
    if (request.method == 'POST'):
        try:
            Query.objects.all().delete()
            KeywordCoordinate.objects.all().delete()
            FinanceQuery.objects.all().delete()
            ChatbotQuery.objects.all().delete()
        except:
            return HttpResponse('ERROR')
        return HttpResponse('OK')
    return render(request,'delete_database.html')