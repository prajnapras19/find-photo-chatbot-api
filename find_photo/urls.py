"""find_photo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from find_photo_by_hashtag.views import find_photo_by_hashtag_views
from find_keyword.views import getMaps, getMapsButton, findKeywordGlobalToSpesific
from find_finance.views import financeToDB, findFinanceRecommendation
from chatbot_recommendation.views import chatbotRecommendation, chatbotToDB
from .views import *

urlpatterns = [
    path('app.smojo.org.ferrysama.prajbot/', admin.site.urls),
    path('app.smojo.org.ferrysama.prajbot.deletedb', deleteDatabase), #use carefully
    
    path('by_hashtag/<str:user>/<str:tag>/<int:request_number>', find_photo_by_hashtag_views),
    
    path('maps/<str:query>', getMaps),
    path('mapbutton/<str:query>', getMapsButton),
    
    path('findKeywordGlobalToSpesific/<str:user>/<str:place_name>/<str:category>/<int:request_number>', findKeywordGlobalToSpesific),
    
    path('uploadcsvfinance', financeToDB),
    path('finance/<str:query>', findFinanceRecommendation),

    path('chatbots/<str:query>', chatbotRecommendation),
    path('uploadcsvchatbot', chatbotToDB),
]
