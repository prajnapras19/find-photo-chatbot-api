from django.db import models

class KeywordCoordinate(models.Model):
    keyword = models.CharField(max_length=100)
    longitude = models.DecimalField(max_digits=9, decimal_places=5)
    latitude = models.DecimalField(max_digits=9, decimal_places=5)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return str(self.keyword)

class KeywordGlobalToSpesific(models.Model):
    user = models.CharField(max_length=100)
    place_name = models.CharField(max_length=100)
    category = models.CharField(max_length=40)
    request_number = models.PositiveIntegerField()
    title = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return str(self.place_name) + ' - ' + str(self.category) + ' - request ' + str(self.request_number) + ' - by ' + str(self.user)