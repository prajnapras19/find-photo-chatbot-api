from django.apps import AppConfig


class FindKeywordConfig(AppConfig):
    name = 'find_keyword'
