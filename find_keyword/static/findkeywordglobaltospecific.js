function loadGlobalToSpecific(name, place_name, category,request_number) {
    name = name.replace(/^\s+|\s+$/g, '');
    place_name = place_name.replace(/^\s+|\s+$/g, '');
    category = category.replace(/\s/g,'');
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            chatBox = document.querySelectorAll(".global-to-specific");
            res = this.responseText;
            let x = 0;
            for (let i = 0; i < chatBox.length; i++) {
                if (x == 0 && !chatBox[i].classList.contains('occupied')) {
                    chatBox[i].innerHTML = res;
                    chatBox[i].className += "occupied";
                    x = 1;
                } else {
                    chatBox[i].innerHTML = '<i>Searching the best recommendation for you. Please wait...</i>&#128512';
                }
            }
        }
    };
    xhttp.open("GET", "https://ferry-chatbot-api.herokuapp.com/findKeywordGlobalToSpesific/" + name + "/" + place_name + "/" + category + "/" + request_number, true);
    xhttp.send();
}