from django.shortcuts import render
from django.http import HttpResponse
import requests
import json
import os
import random
from django.views.decorators.clickjacking import xframe_options_exempt
from find_keyword.models import KeywordCoordinate, KeywordGlobalToSpesific
from find_photo_by_hashtag.views import get_posts_by_hashtags, parse_to_url
#Additional modules by toni
from geopy.geocoders import Nominatim
from requests_oauthlib import OAuth1

errorResponse = '''Sorry, we don't have any suggestions about that right now. Please come back later or input something else &#128513;'''

def findCoordinate(query):
    query = query[:]
    query += ' indonesia' # tambahkan indonesia agar bisa dapat query di indonesia
    coordinate = KeywordCoordinate.objects.filter(keyword=query)
    if (len(coordinate) > 0):
        return [coordinate[0].longitude, coordinate[0].latitude]
    try:
        url = 'http://dev.virtualearth.net/REST/v1/Locations/%s' % query
        params = {
            'key': os.getenv('BINGMAPSKEY'),
        }
        res = requests.get(url, params=params)
        res = json.loads(res.content)
        res = res['resourceSets'][0]['resources'][0]['point']['coordinates']
        KeywordCoordinate.objects.create(keyword=query, longitude=res[0], latitude=res[1])
        return res
    except:
        return None
    
def getCoordinate_geopy(place_name): #input string, kyk google search
    coordinate = KeywordCoordinate.objects.filter(keyword=place_name)
    if (len(coordinate) > 0):
        return [coordinate[0].longitude, coordinate[0].latitude]

    geolocator = Nominatim(user_agent="Prajbot")
    try:
        search=geolocator.geocode(place_name).raw
        coordinate=search['lat'],search['lon']
        KeywordCoordinate.objects.create(keyword=place_name, longitude=coordinate[0], latitude=coordinate[1])
        return coordinate #return (latitude,longitude)
    except:
        return None

def getProvAndCity(coordinate): #input (latitude,longitude)
    try:
        # 0. Prep
        coor=','.join(list(map(str,coordinate)))
        search_query = 'https://discover.search.hereapi.com/v1/discover?at='+str(coor)+'&q=indonesia'
        #OAuth 2.0 (JSON Web Tokens) punya toni
        kp=[None,os.getenv('clientID'),os.getenv('clientSecret')]

        # 1. Retrieve token
        data = {
            'grantType': 'client_credentials',
            'clientId': kp[1],
            'clientSecret': kp[2]
            }

        response = requests.post(
            url='https://account.api.here.com/oauth2/token',
            auth=OAuth1(kp[1], client_secret=kp[2]) ,
            headers= {'Content-type': 'application/json'},
            data=json.dumps(data)).json()

        token = response['accessToken']
        token_type = response['tokenType']
        expire_in = response['expiresIn']
    except:
        return None

    # 2. Use it in HERE Geocoding And Search query header
    try:
        headers = {'Authorization': f'{token_type} {token}'}
        search_results = requests.get(search_query, headers=headers).json()
        LOD=search_results["items"]
        # Ambil 1 tempat dengan lokasi terdekat
        LOD.sort(key=lambda x:x["distance"])
        nearest_result=LOD[0]["address"]
        #return prov,kota
        prov,kota=nearest_result["county"],nearest_result["city"]
        return prov,kota #return (prov,kota)
    except:
        return None


@xframe_options_exempt
def getMaps(request, query=None):
    response = {}
    if (query == None):
        return render(request, 'find_keyword/maps.html', response)
    coordinate = findCoordinate(query)
    if (coordinate == None):
        return render(request, 'find_keyword/maps.html', response)
    
    response['coordinate'] = coordinate
    return render(request, 'find_keyword/maps.html', response)

def getMapsButton(request, query=None):
    response = {}
    if (query == None):
        return render(request, 'find_keyword/mapsbutton.html', response)
    coordinate = findCoordinate(query)
    if (coordinate == None):
        return render(request, 'find_keyword/mapsbutton.html', response)

    response['coordinate'] = coordinate
    return render(request, 'find_keyword/mapsbutton.html', response)

def findPlacesAroundByCoordinate(coordinate, category):
    try:
        url = 'https://places.ls.hereapi.com/places/v1/discover/explore'
        params = {
            'apiKey': os.getenv('HEREAPIKEY'),
            'cat': category,
            'at': ','.join(list(map(str,coordinate))),
        }
        res = requests.get(url, params=params)
        res = json.loads(res.content)
        res = [x for x in res['results']['items']]
        res = random.sample(res, min(len(res), 2))
        return res
    except:
        return []

def findKeywordGlobalToSpesific(request, user, place_name, category, request_number):
    categories = ['eat-drink', 'going-out,sights-museums', 'shopping', 'leisure-outdoor,natural-geographical']
    category = category.lower()
    if (category not in categories or request_number < 1):
        return HttpResponse(errorResponse)
    response = {
        'place_name': place_name,
        'places': [],
    }

    inDatabase = False
    # check if it exists in database, otherwise do search
    keywordGlobalToSpesific = KeywordGlobalToSpesific.objects.filter(user=user, place_name=place_name, category=category, request_number=request_number)
    if (len(keywordGlobalToSpesific) > 0):
        inDatabase = True
        places = [{'title':x} for x in keywordGlobalToSpesific[0].title.split(';')]
    else:
        coordinate = findCoordinate(place_name)
        if (coordinate == None):
            return HttpResponse(errorResponse)

        places = findPlacesAroundByCoordinate(coordinate, category)
        if (len(places) == 0):
            return HttpResponse(errorResponse)

    titleList = []
    for place in places:
        titleList.append(place['title'])

        # find photo
        tag = place['title']
        posts = get_posts_by_hashtags(user, tag, request_number)
        if (posts != None):
            display_urls = [post[1]['display_url'] for post in posts]
            shortcodes = parse_to_url([post[0] for post in posts])
            tmp = []
            for i in range(len(display_urls)):
                tmp.append((display_urls[i], shortcodes[i]))
            response['places'].append({'name':place['title'], 'posts': tmp[:1]})
        else:
            response['places'].append({'name':place['title']})

    # add to database
    if (not inDatabase):
        KeywordGlobalToSpesific.objects.create(user=user, place_name=place_name, category=category, request_number=request_number, title=';'.join(titleList))
    #print(response)
    return render(request, 'find_keyword/global_to_specific.html',response)


    
    
    
