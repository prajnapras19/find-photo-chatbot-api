from django.contrib import admin
from find_keyword.models import KeywordCoordinate, KeywordGlobalToSpesific

class CreatedAtAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at',)

admin.site.register(KeywordCoordinate, CreatedAtAdmin)
admin.site.register(KeywordGlobalToSpesific, CreatedAtAdmin)